#include <stdio.h>




//defining the structure for student record
struct student {
    char firstName[50];
    char subject[50];
    float marks;
} s[5];



int displayInformation(){

    // displaying information
    
    printf("Displaying Information:\n\n");


    for (int i = 0; i < 5; ++i) {
        printf("\nSubject: ");
        puts(s[i].subject);
        printf("First name: ");
        puts(s[i].firstName);
        printf("Marks: %.1f", s[i].marks);
        printf("-------------------------\n\n");
    }


}


int storeinformation(){

    // storing information
    for (int i = 0; i < 5; ++i) {
        printf("Subject: ");
        scanf("%s", s[i].subject);
        printf("Enter first name: ");
        scanf("%s", s[i].firstName);
        printf("Enter marks: ");
        scanf("%f", &s[i].marks);
        printf("-------------------------\n\n");
    }
    


}



int main() {


while(1){
  int option ;
  printf("Enter the option : \n");
  printf("1. Enter student details : \n");
  printf("2. List student details : \n");
  printf("3. Exit : \n\n");
  printf("Please enter your choice 1/2/3 : \n");
  scanf("%i", &option);
  
  switch (option){
    case 1:
	storeinformation();
      break;
    case 2:
	displayInformation();
      break;    
    case 3:
	return 0 ;
      break;
    default:
        printf("option unknows exiting the program : \n");
}
  
  
  
}


    return 0;
}



